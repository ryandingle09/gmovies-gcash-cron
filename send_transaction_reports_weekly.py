
import os
from google.cloud import datastore
import xlwt
import json
import calendar
import time
import sys
import xlwt
import StringIO
from datetime import time, datetime, timedelta, date
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import smtplib  
import email.utils

ALLOWED_EXTENSIONS = set(['csv', 'txt'])
ALLOWED_IMAGE_CONTENT_TYPE = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']
ISO_WEEKDAY = {'1': 'Monday', '2': 'Tuesday', '3': 'Wednesday', '4': 'Thursday', '5': 'Friday', '6': 'Saturday', '7': 'Sunday'}

credentials = '/home/ubuntu/cronjobs/credentials/globe-gmovies-10cb55ea5ff3.json'
project_id  = 'globe-gmovies'
url         = 'https://globe-gmovies.appspot.com/admin/transactions'

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials
projectID = project_id

def convert_timezone(date_parameter, hours, operation):
    if operation == '+':
        result = date_parameter + timedelta(hours=hours)
    else:
        result = date_parameter - timedelta(hours=hours)

    return result

def parse_date_to_string(ddate, fdate='%B %d, %Y'):
    sdate = None
    try:
        sdate = date.strftime(ddate, fdate)
    except ValueError, e:
        print e
    except Exception, e:
        print e
    return sdate

def parse_time_to_string(ttime, ftime='%I:%M:%S %p'):
    stime = None
    try:
        stime = time.strftime(ttime, ftime)
    except ValueError, e:
        print e
    except Exception, e:
        print e
    return stime

def parse_string_to_datetime(sdatetime, fdatetime='%Y-%m-%d %H:%M:%S'):
    ddatetime = None
    try:
        ddatetime = datetime.strptime(sdatetime, fdatetime)
    except ValueError, e:
        print e
    except Exception, e:
        print e
    return ddatetime

def get_transaction_time_cluster(hour):
    cluster = ''
    if hour in [0, 1, 2, 3, 4, 5]:
        cluster = 'B1'
    elif hour in [6, 7, 8, 9]:
        cluster = 'B2'
    elif hour in [10, 11, 12, 13]:
        cluster = 'B3'
    elif hour in [14, 15, 16, 17]:
        cluster = 'B4'
    elif hour in [18, 19, 20]:
        cluster = 'B5'
    elif hour in [21, 22, 23]:
        cluster = 'B6'
    return cluster

def get_ticket_time_cluster(hour):
    cluster = ''
    if hour in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
        cluster = 'S1'
    elif hour in [10, 11, 12, 13, 14]:
        cluster = 'S2'
    elif hour in [15, 16, 17]:
        cluster = 'S3'
    elif hour in [18, 19, 20]:
        cluster = 'S4'
    elif hour in [21, 22, 23]:
        cluster = 'S5'
    return cluster

def week_range(date):
    """Find the first/last day of the week for the given day.
    Assuming weeks start on Sunday and end on Saturday.

    Returns a tuple of ``(start_date, end_date)``.

    """
    # isocalendar calculates the year, week of the year, and day of the week.
    # dow is Mon = 1, Sat = 6, Sun = 7
    year, week, dow = date.isocalendar()

    # Find the first day of the week.
    if dow == 7:
        # Since we want to start with Sunday, let's test for that condition.
        start_date = date
    else:
        # Otherwise, subtract `dow` number days to get the first day
        start_date = date - timedelta(dow)

    # Now, add 6 for the last day of the week (i.e., count up to Saturday)
    end_date = start_date + timedelta(6)

    return (datetime.combine(start_date, time()), datetime.combine(end_date, time(23, 59, 59)))

def generate_transaction_reports_xls(tx_from, tx_to):
    tx_from = convert_timezone(tx_from, 152, '-') # convert to UTC.
    tx_to = convert_timezone(tx_to, 152, '-') # convert to UTC.

    client = datastore.Client(projectID)

    payment_type = 'gcash'
    payment_type2 = 'g-cash-app'

    print 'db_from : %s' % tx_from
    print 'db_to : %s' % tx_to

    query = client.query(kind='ReservationTransaction')
    #query.add_filter('payment_type', '=', 'g-cash-app')
    #query.add_filter('payment_type', '=', 'gcash')
    query.add_filter('state', '=', 100)
    query.add_filter('date_created', '>=', tx_from)
    query.add_filter('date_created', '<=', tx_to)

    # generate xls file
    row_counter = 0
    workbook = xlwt.Workbook(encoding='utf-8')
    worksheet = workbook.add_sheet("Sheet 1")
    headers_font_style = xlwt.XFStyle()
    headers_font_style.font.bold = True
    font_style = xlwt.XFStyle()
    font_style.font.bold = False

    worksheet.write(0, 0, 'WEEK NUMBER', headers_font_style)
    worksheet.write(0, 1, 'TRANSACTION DAY', headers_font_style)
    worksheet.write(0, 2, 'TRANSACTION DATE', headers_font_style)
    worksheet.write(0, 3, 'TRANSACTION TIME', headers_font_style)
    worksheet.write(0, 4, 'MOVIE TITLE', headers_font_style)
    worksheet.write(0, 5, 'CINEMA PARTNER', headers_font_style)
    worksheet.write(0, 6, 'CINEMA NUMBER', headers_font_style)
    worksheet.write(0, 7, 'TICKET SOLD', headers_font_style)
    worksheet.write(0, 8, 'NET SALES', headers_font_style)
    worksheet.write(0, 9, 'TICKET DATE', headers_font_style)
    worksheet.write(0, 10, 'TICKET TIME', headers_font_style)
    worksheet.write(0, 11, 'PROMO_CODE', headers_font_style)
    worksheet.write(0, 12, 'PROMO DISCOUNT', headers_font_style)
    worksheet.write(0, 13, 'RESERVATION CODE', headers_font_style)

    for transaction in query.fetch():

        if transaction['payment_type'] == 'gcash' or transaction['payment_type'] == 'g-cash-app':

            row_counter += 1
            movie_title = ''
            cinema_partner = ''
            cinema_number = ''
            mobile_number = 'N/A'
            gross_sales = ''
            promo_code = ''
            promo_discount = ''

            ticket_extra = json.loads(transaction['ticket.extra']) if ('ticket.extra' in transaction and transaction['ticket.extra']) else {}
            payment_info = json.loads(transaction['payment_info']) if ('payment_info' in transaction and transaction['payment_info']) else {}
            user_info = json.loads(transaction['user_info']) if ('user_info' in transaction and transaction['user_info']) else {}
            discount_info = json.loads(transaction['discount_info']) if ('discount_info' in transaction and transaction['discount_info']) else {}

            if 'movie_canonical_title' in ticket_extra and ticket_extra['movie_canonical_title']:
                movie_title = ticket_extra['movie_canonical_title']
            elif 'movie_title' in ticket_extra and ticket_extra['movie_title']:
                movie_title = ticket_extra['movie_title']

            if 'theater_name' in ticket_extra and ticket_extra['theater_name']:
                cinema_partner = ticket_extra['theater_name']
            elif 'theater_and_cinema_name' in ticket_extra and ticket_extra['theater_and_cinema_name']:
                cinema_partner = ticket_extra['theater_and_cinema_name'][0].split('-')[0].strip()

            if 'cinema_name' in ticket_extra and ticket_extra['cinema_name']:
                cinema_number = ticket_extra['cinema_name']
            elif 'theater_and_cinema_name' in ticket_extra and ticket_extra['theater_and_cinema_name']:
                cinema_number = ticket_extra['theater_and_cinema_name'][0].split('-')[-1].replace('Cinema:', '').strip()

            if len(cinema_number) <= 2:
                cinema_number = 'Cinema %s' % cinema_number

            if 'total_amount' in ticket_extra and ticket_extra['total_amount']:
                gross_sales = ticket_extra['total_amount']

            if discount_info and 'claim_code' in discount_info and discount_info['claim_code']:
                promo_code = discount_info['claim_code']

            if payment_info and 'promo_code' in payment_info and payment_info['promo_code']:
                claim_code = payment_info['promo_code']

            if discount_info and 'total_discount' in discount_info and discount_info['total_discount']:
                promo_discount = discount_info['total_discount']

            transaction_datetime = convert_timezone(transaction['date_created'], 8, '+')
            isoyear, isoweek, isoday = transaction_datetime.isocalendar()

            week_number = isoweek
            transaction_day = ISO_WEEKDAY[str(isoday)]
            transaction_date = parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
            transaction_time = parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')
            transaction_time_cluster = get_transaction_time_cluster(transaction_datetime.time().hour) if transaction_datetime else ''

            ticket_datetime = None

            if 'show_datetime' in ticket_extra and ticket_extra['show_datetime']:
                ticket_datetime = parse_string_to_datetime(ticket_extra['show_datetime'], '%m/%d/%Y %I:%M %p')

            ticket_date = parse_date_to_string(ticket_datetime.date(), fdate='%m/%d/%Y') if ticket_datetime else ''
            ticket_time = parse_time_to_string(ticket_datetime.time(), ftime='%I:%M %p') if ticket_datetime else ''
            ticket_time_cluster = get_ticket_time_cluster(ticket_datetime.time().hour) if ticket_datetime else ''

            worksheet.write(row_counter, 0, week_number, font_style)
            worksheet.write(row_counter, 1, transaction_day, font_style)
            worksheet.write(row_counter, 2, transaction_date, font_style)
            worksheet.write(row_counter, 3, transaction_time, font_style)
            worksheet.write(row_counter, 4, movie_title, font_style)
            worksheet.write(row_counter, 5, cinema_partner, font_style)
            worksheet.write(row_counter, 6, cinema_number, font_style)
            worksheet.write(row_counter, 7, len(transaction['reservations.theater']), font_style)
            worksheet.write(row_counter, 8, gross_sales, font_style)
            worksheet.write(row_counter, 9, ticket_date, font_style)
            worksheet.write(row_counter, 10, ticket_time, font_style)
            worksheet.write(row_counter, 11, promo_code, font_style)
            worksheet.write(row_counter, 12, promo_discount, font_style)
            worksheet.write(row_counter, 13, transaction['reservation_reference'], font_style)

    tx_reports = StringIO.StringIO()
    workbook.save(tx_reports)

    file_name = 'GMoviesTransactionReports-Weekly'
    if payment_type:
        file_name = file_name + ("-%s-&-%s" % (payment_type.upper(), payment_type2.upper()))
    file_name = file_name + ".xls"

    return file_name, tx_reports.getvalue()

def send_email(send_from, sender_name, send_to_text, send_to_destinations, send_to_bcc_text):
    # convert to Philippine time (GMT +8)
    today = datetime.today() + timedelta(hours=8)
    tx_from, tx_to = week_range(today)
    file_name, file_data =  generate_transaction_reports_xls(tx_from, tx_to)

    print 'today %s' % today
    print tx_from
    print tx_to

    USERNAME_SMTP = "AKIAJ4FCUVESBIMVG2RQ"
    PASSWORD_SMTP = "ApJSZdLm9Je34QdxcIVQ1Gqj00qk3ZzDUkmmQVQoC8c8"
    HOST = "email-smtp.us-east-1.amazonaws.com"
    PORT = 25
    SUBJECT = "Gcash Transaction Reports Weekly - %s %s" % (datetime.now().date().strftime("%B"), datetime.now().year)

    msg = MIMEMultipart('mixed')
    msg['Subject'] = SUBJECT
    msg['Bcc'] = send_to_bcc_text
    msg['From'] = email.utils.formataddr((sender_name, send_from))
    msg['To'] = send_to_text
    # What a recipient sees if they don't use an email reader
    msg.preamble = 'Multipart message.\n'

    mail_body = """
    <p>%s</p>
    """ % (SUBJECT)

    msgAlternative = MIMEMultipart('alternative')
    msg.attach(msgAlternative)

    msgText = MIMEText(mail_body, 'html')
    msgAlternative.attach(msgText)

    # The attachment
    part = MIMEApplication(file_data)
    part.add_header('Content-Disposition', 'attachment', filename=file_name)
    part.add_header('Content-Type', 'application/vnd.ms-excel; charset=UTF-8')
    msg.attach(part)

    try:  
        server = smtplib.SMTP(HOST, PORT)
        server.ehlo()
        server.starttls()
        #stmplib docs recommend calling ehlo() before & after starttls()
        server.ehlo()
        server.login(USERNAME_SMTP, PASSWORD_SMTP)
        server.sendmail(send_from, send_to_destinations, msg.as_string())
        server.close()
    except Exception as e:
        print ("Error: ", e)
    else:
        print ("Email sent!")

send_from = "support@gmovies.ph"
sender_name = 'GMovies Transaction Reports'
send_to_text = 'mix.martinez@mynt.xyz, zmtdeguzman@globe.com.ph'
send_to_text_bcc = 'eiturralde@yondu.com, zjgtorio@globe.com.ph, rdingle@yondu.com'
#send_to_text = 'rdingle@yondu.com'
#send_to_text_bcc = 'ryandingle09@gmail.com, rldwebshop@gmail.com'
#send_to_destinations = send_to_text_bcc.split(",") + ['rdingle@yondu.com']
send_to_destinations = send_to_text_bcc.split(",") + ['mix.martinez@mynt.xyz', 'zmtdeguzman@globe.com.ph']
send_email(send_from, sender_name, send_to_text, send_to_destinations, send_to_text_bcc)